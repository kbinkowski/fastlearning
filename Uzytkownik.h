#ifndef UZYTKOWNIK_H
#define UZYTKOWNIK_H
#include "KursProxy.h"
#include "Stan.h"
#ifndef Q_MOC_RUN
#include <boost/shared_ptr.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/foreach.hpp>
#endif //Q_MOC_RUN
typedef boost::shared_ptr<KursProxy> PKursProxy;
/**
 * Klasa reprezentuje Użytkownika
 */
class Uzytkownik
{
public:
    Uzytkownik();
    Uzytkownik(const string sciezka);
    vector<PKursProxy> zwrocNieaktywneKursy();
    vector<PKursProxy> zwrocAktywneKursy();
    void nowyKurs (string sciezka, string nazwa);
    void aktywujKurs (const int indeks, int iloscPytan);
    void deaktywujKurs(const int indeks);
    void aktualizujKursy();
    void ocenZapytanie(const int ocena);
    void usunKurs(const int indeks);
    PZapytanie zwrocKolejnePytnie();
    PZapytanie zwrocPytanie(int kurs, int indeks);
    const int zwrocIloscPytan(const int indeks, bool aktywny);
    const vector<PZapytanie> szukajPytanie(const string wzor);
    const multimap<date,PZapytanie> zwrocTerminarz(const int kurs);

private:
    PZapytanie zwrocPowtorzenie();
    PZapytanie zwrocNauka();
    PZapytanie zwrocUtrwalenie();

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version) {
            ar & sciezka_;
            ar & kursyNieaktywne_;
            ar & kursyAktywne_;
        }

    vector<PKursProxy> kursyNieaktywne_;
    vector<PKursProxy> kursyAktywne_;
    string sciezka_;
    Stan stan_;
    int indeksKurs_;
    int indeksPytanie_;
    int aktualnyKurs_;
};

#endif // UZYTKOWNIK_H
