#include "Uzytkownik.h"
using namespace std;
Uzytkownik::Uzytkownik(){
    stan_=POWTORZENIE;
    indeksKurs_=0;
}

Uzytkownik::Uzytkownik(const string sciezka){
    sciezka_=sciezka;
    stan_=POWTORZENIE;
    indeksKurs_=0;
}


vector<PKursProxy> Uzytkownik::zwrocNieaktywneKursy(){
    return kursyNieaktywne_;
}

void Uzytkownik::nowyKurs(string sciezka, string nazwa){
    kursyNieaktywne_.push_back(PKursProxy(new KursProxy(sciezka, nazwa)));
}

void Uzytkownik::aktywujKurs(const int indeks, int iloscPytan){
    kursyNieaktywne_[indeks]->ustawIloscNowychPytan(iloscPytan);
    kursyAktywne_.push_back(kursyNieaktywne_[indeks]);
    kursyNieaktywne_.erase(kursyNieaktywne_.begin()+indeks);
}

void Uzytkownik::deaktywujKurs(const int indeks){
    kursyNieaktywne_.insert(kursyNieaktywne_.begin()+indeks, kursyAktywne_[indeks]);
    kursyNieaktywne_[indeks]->ustawIloscNowychPytan(0);
    kursyAktywne_.erase(kursyAktywne_.begin()+indeks);
}

vector<PKursProxy> Uzytkownik::zwrocAktywneKursy(){
    return kursyAktywne_;
}

void Uzytkownik::aktualizujKursy(){
    stan_=POWTORZENIE;
    indeksKurs_=0;
    indeksPytanie_=0;
    aktualnyKurs_=0;
    BOOST_FOREACH(PKursProxy &v, kursyAktywne_){ v->aktualizuj();}
}

void Uzytkownik::ocenZapytanie(const int ocena){
    kursyAktywne_[aktualnyKurs_]->ocenZapytanie(ocena);
}

void Uzytkownik::usunKurs(const int indeks){
    kursyNieaktywne_.erase(kursyNieaktywne_.begin()+indeks);
}

PZapytanie Uzytkownik::zwrocPytanie(int kurs, int indeks){
    return kursyAktywne_[kurs]->zapytanie(indeks);
}

const int Uzytkownik::zwrocIloscPytan(const int indeks, bool aktywny){
    if (aktywny)
        return kursyAktywne_[indeks]->iloscZapytan();
    else
        return kursyNieaktywne_[indeks]->iloscZapytan();
}

const vector<PZapytanie> Uzytkownik::szukajPytanie(const string wzor){
    vector<PZapytanie> sumaZnalezionych;
    vector<PZapytanie> znalezioneWKursie;
    BOOST_FOREACH(PKursProxy v, kursyAktywne_){
        znalezioneWKursie=v->szukajPytanie(wzor);
        sumaZnalezionych.insert(sumaZnalezionych.end(),znalezioneWKursie.begin(),znalezioneWKursie.end());
    }
    return sumaZnalezionych;
}

const multimap<date,PZapytanie> Uzytkownik::zwrocTerminarz(const int kurs){
    return kursyAktywne_[kurs]->zwrocTerminarz();
}


PZapytanie Uzytkownik::zwrocKolejnePytnie(){
    PZapytanie pytanie;
    switch(stan_){

       case POWTORZENIE:
            pytanie = zwrocPowtorzenie();
            if (pytanie.get()!=NULL){
                aktualnyKurs_=indeksKurs_;
                return pytanie;
            }
            stan_=NAUKA;
            indeksKurs_=0;

       case NAUKA:
            pytanie = zwrocNauka();
            if (pytanie.get()!=NULL){
                aktualnyKurs_=indeksKurs_;
                if (indeksPytanie_==kursyAktywne_[indeksKurs_]->ileNowychPytan()){
                    indeksKurs_++;
                    indeksPytanie_=0;
                }
                return pytanie;
            }
            stan_=UTRWALENIE;
            indeksKurs_=0;
            indeksPytanie_=0;

       case UTRWALENIE:
            pytanie = zwrocUtrwalenie();
            aktualnyKurs_=indeksKurs_;
            return pytanie;

       default:
       break;
   }
    return pytanie;
}

PZapytanie Uzytkownik::zwrocPowtorzenie(){
    PZapytanie pytanie;
    int koniec = kursyAktywne_.size();
    date dzis = day_clock::local_day();
    for (int i = indeksKurs_; i!=koniec; ++i){;
        pytanie=kursyAktywne_[i]->kolejnePowtorka(dzis);
        if (pytanie.get()!=NULL){
            indeksKurs_=i;
            return pytanie;
        }
    }
    return pytanie;
}

PZapytanie Uzytkownik::zwrocNauka(){
    PZapytanie pytanie;
    int koniec = kursyAktywne_.size();
    for (int i = indeksKurs_; i!=koniec; ++i){
        pytanie=kursyAktywne_[i]->kolejneNauka();
        if (pytanie.get()!=NULL){
            indeksKurs_=i;
            ++indeksPytanie_;
            return pytanie;
        }
    }
    return pytanie;
}

PZapytanie Uzytkownik::zwrocUtrwalenie(){
    PZapytanie pytanie;
    int koniec = kursyAktywne_.size();
    for (int i = indeksKurs_; i!=koniec; ++i){
        pytanie=kursyAktywne_[i]->kolejneUtrwalanie();
        if (pytanie.get()!=NULL){
            indeksKurs_=i;
            return pytanie;
        }
    }
    return pytanie;
}
