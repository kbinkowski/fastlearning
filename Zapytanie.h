#ifndef ZAPYTANIE_H
#define ZAPYTANIE_H
#include <string>
#ifndef Q_MOC_RUN
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/serialization/nvp.hpp>
#endif // Q_MOC_RUN

using std::string;
using boost::gregorian::date;
using boost::gregorian::day_clock;
using boost::gregorian::date_duration;
/**
 *Klasa reprezentuje zapytanie, zaweirajace pytanie oraz odpowiedz, a takze zmienne
 * sluzace do obliczania kolejnej daty wyswietlenia.
 */
class Zapytanie{

public:
    Zapytanie(){}
    Zapytanie(const string pytanie, const string odpowiedz);
    const Zapytanie& operator=(const Zapytanie& wzor);
    const string dajPytanie();
    const string dajOdpowiedz();
    const date ocenPytanie(const int ocena);
    double dajWspolczynnik();
    double dajInterwal();

private:
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version) {
        ar & pytanie_;
        ar & odpowiedz_;
        ar & interwal;
        ar & wspolczynnik;        
    }

     string pytanie_;
     string odpowiedz_;
     //Interwał od poprzedniego pytania
     double interwal;
     //Współczynnik trudności pytania
     double wspolczynnik;
};

#endif // ZAPYTANIE_H

