#include "Kurs.h"
#include <iostream>
using namespace std;
Kurs::Kurs(const string sciezka){
    string pytanie;
    string odpowiedz;
    ifstream plik(sciezka.c_str());
    if (plik.is_open()){
        while (!plik.eof()){
            getline(plik, pytanie);
            getline(plik, odpowiedz);
            PZapytanie p = PZapytanie(new Zapytanie(pytanie, odpowiedz));
            zapytania_.push_back(p);
            naukaZapytania_.push_back(p);
            getline(plik, pytanie);
        }
    }
    plik.close();
}

const PZapytanie Kurs::zapytanie(const int i){
    return zapytania_[i];
}
const PZapytanie Kurs::kolejneNauka(){
    aktualneZapytanie_=NULL;
    if (naukaZapytania_.size()!=0){
        aktualneZapytanie_=naukaZapytania_.front();
        naukaZapytania_.pop_front();
        return aktualneZapytanie_;
    }
    else
        return aktualneZapytanie_;
}
const PZapytanie Kurs::kolejneUtrwalanie(){
    aktualneZapytanie_=NULL;
    if (utrwalanieZapytania_.size()){
        aktualneZapytanie_=utrwalanieZapytania_.front();
        utrwalanieZapytania_.pop_front();
        return aktualneZapytanie_;
    }
    else
        return aktualneZapytanie_;

}
const PZapytanie Kurs::kolejnePowtorka(const date data){
    aktualneZapytanie_=NULL;
    if (powtorkaZapytania_.size()){
        multimap<date, PZapytanie>::const_iterator iterator = powtorkaZapytania_.find(data);
        if(iterator!=powtorkaZapytania_.end()){
            aktualneZapytanie_=iterator->second;
            powtorkaZapytania_.erase(iterator);
        }
        return aktualneZapytanie_;
    }
    else
        return aktualneZapytanie_;
}

void Kurs::ocenZapytanie(const int ocena){
    if(ocena<3)
         utrwal(aktualneZapytanie_);
    else{
        date d=aktualneZapytanie_->ocenPytanie(ocena);
        powtorz(d,aktualneZapytanie_);
     }
}
const int Kurs::iloscZapytan(){
    return zapytania_.size();
}

void Kurs::ustawIloscNowychPytan(const int ilosc){
    iloscNowychPytan_=ilosc;
}

const int Kurs::ileNowychPytan(){
    return iloscNowychPytan_;
}

const vector<PZapytanie> Kurs::szukajPytanie(const string wzor){

    vector<PZapytanie> znalezione;
    boost::regex reg (".*"+wzor+".*");
    BOOST_FOREACH(PZapytanie v, zapytania_){

    if(regex_match(v->dajPytanie(), reg))
        znalezione.push_back(v);
    }
    return znalezione;
}
const multimap<date,PZapytanie> Kurs::zwrocTerminarz(){
    return powtorkaZapytania_;
}

void Kurs::utrwal(PZapytanie doUtrwalenia){
    utrwalanieZapytania_.push_back(doUtrwalenia);
}
void Kurs::powtorz(date data, PZapytanie doPowtorki){
    powtorkaZapytania_.insert(std::pair<date,PZapytanie>(data,doPowtorki));
}


void Kurs::aktualizuj(){
    if(!powtorkaZapytania_.empty()){
        date dzis=day_clock::local_day();
        date wpisywane=dzis;
        pair<date,PZapytanie> c;
        multimap<date,PZapytanie>::iterator i =powtorkaZapytania_.begin();
        int temp =0;
        c = *i;
        date sprawdzane = c.first;
        while(sprawdzane<dzis){

            c = std::pair<date,PZapytanie> (wpisywane,c.second);
            powtorkaZapytania_.insert(c);
            powtorkaZapytania_.erase(i);
            ++temp;
            ++i;

            c = *i;

            sprawdzane=c.first;
            if(temp==iloscNowychPytan_-1){
                temp=0;
                wpisywane+=date_duration(1);
            }
        }
    }
}

