#include "Widok.h"
#include "Kontroler.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Kontroler* k = new Kontroler();
    Widok* w = new Widok(k);
    k->dodajWidok(w);
    return a.exec();
}
