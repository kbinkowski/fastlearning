#include "UzytkownikProxy.h"


UzytkownikProxy::UzytkownikProxy(string sciezka, bool import){
    unsigned f1 = sciezka.find_last_of("//");
    unsigned f2 = sciezka.find_last_of(".");
    if (import){
        nazwa_=sciezka.substr(f1+1,f2-f1-1);
        plikSerializacji_=sciezka;
        try{
            std::ifstream ifs(plikSerializacji_.c_str());
            boost::archive::text_iarchive ia(ifs);
            ia >> plikSerializacji_>>nazwa_>>uzytkownik_;
            ifs.close();
        }
        catch(...){
            throw Wyjatek("Błąd przy odczycie użytkownika");
        }
    }
    else{
        nazwa_=sciezka;
        plikSerializacji_="Uzytkownik_"+nazwa_+".user";
    } 
}

void UzytkownikProxy::eksportuj(){
    try{
        ofstream ofs(plikSerializacji_.c_str());
        boost::archive::text_oarchive oa(ofs);
        oa << plikSerializacji_ <<nazwa_ <<uzytkownik_;
        ofs.close();
    }
    catch(...){
            throw Wyjatek("Błąd przy zapisie użytkownika");
    }
}

PUzytkownik UzytkownikProxy::zwrocUzytkownika(){
    if(!uzytkownik_){
        uzytkownik_ = PUzytkownik(new Uzytkownik(nazwa_));
    }
    return uzytkownik_;
}

string UzytkownikProxy::zwrocNazwe(){
    return nazwa_;
}

void UzytkownikProxy::nowyKurs(string sciezka){
    zwrocUzytkownika()->nowyKurs(sciezka, nazwa_);
}

void UzytkownikProxy::aktywujKurs(const int indeks, const int iloscPytan){
    zwrocUzytkownika()->aktywujKurs(indeks,iloscPytan);
}

void UzytkownikProxy::deaktywujKurs(const int indeks){
    zwrocUzytkownika()->deaktywujKurs(indeks);
}

vector<PKursProxy> UzytkownikProxy::zwrocNieaktywneKursy(){
    return zwrocUzytkownika()->zwrocNieaktywneKursy();
}
vector<PKursProxy> UzytkownikProxy::zwrocAktywneKursy(){
    return zwrocUzytkownika()->zwrocAktywneKursy();
}
void UzytkownikProxy::aktualizujKursy(){
    zwrocUzytkownika()->aktualizujKursy();
}
void UzytkownikProxy::ocenZapytanie(const int ocena){
    zwrocUzytkownika()->ocenZapytanie(ocena);
}
void UzytkownikProxy::usunKurs(const int indeks){
    zwrocUzytkownika()->usunKurs(indeks);
}

PZapytanie UzytkownikProxy::zwrocKolejnePytanie(){
    return zwrocUzytkownika()->zwrocKolejnePytnie();
}

PZapytanie UzytkownikProxy::zwrocPytanie(int kurs, int indeks){
    return zwrocUzytkownika()->zwrocPytanie(kurs, indeks);
}

const int UzytkownikProxy::zwrocIloscPytan(const int indeks, bool aktywny){
    return zwrocUzytkownika()->zwrocIloscPytan(indeks,aktywny);
}

const vector<PZapytanie> UzytkownikProxy::szukajPytanie(const string wzor){
    return zwrocUzytkownika()->szukajPytanie(wzor);
}

const multimap<date,PZapytanie> UzytkownikProxy::zwrocTerminarz(const int kurs){
    return zwrocUzytkownika()->zwrocTerminarz(kurs);
}

