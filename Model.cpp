#include "Model.h"
#include "UzytkownikProxy.h"



Model::Model(){
   try{
        ifstream ifs("model.config");
        boost::archive::text_iarchive ia(ifs);
        ia>> uzytkownicy_;
        ifs.close();
   }
   catch(...){
    }
}


void Model::stworzNowyUzytkownik(const string login){
    if (aktualnyUzytkownik_){
        aktualnyUzytkownik_->eksportuj();
    }
    aktualnyUzytkownik_=PUzytkownikProxy(new UzytkownikProxy(login));
    uzytkownicy_.push_back(aktualnyUzytkownik_);
}

void Model::stworzImportUzytkownik(const string sciezka){
    if (aktualnyUzytkownik_){
        aktualnyUzytkownik_->eksportuj();
    }
    PUzytkownikProxy importUzytkownik(new UzytkownikProxy(sciezka, true));
    aktualnyUzytkownik_=importUzytkownik;
    uzytkownicy_.push_back(aktualnyUzytkownik_);
}


void Model::wybierzIstniejacyUzytkownik(const int indeks){
    if (aktualnyUzytkownik_){
        aktualnyUzytkownik_->eksportuj();
    }
    aktualnyUzytkownik_=uzytkownicy_[indeks];
}

const vector<string> Model::zwrocListaAktywnychKursow(){
    vector<string> vc;
    vector<PKursProxy> aktywne = aktualnyUzytkownik_->zwrocAktywneKursy();
    BOOST_FOREACH(PKursProxy &v, aktywne){vc.push_back(v->nazwa());}
    return vc;
}

const vector<string> Model::zwrocListaNieaktywnychKursow(){
    vector<string> vc;
    vector<PKursProxy> nieaktywne = aktualnyUzytkownik_->zwrocNieaktywneKursy();
    BOOST_FOREACH(PKursProxy &v, nieaktywne){vc.push_back(v->nazwa());}
    return vc;

}

const vector<string> Model::zwrocListaUzytkownikow(){
    vector<string> vc;
    BOOST_FOREACH(PUzytkownikProxy &v, uzytkownicy_){vc.push_back(v->zwrocNazwe());}
    return vc;
}

const pair<string,string> Model::zwrocPytanie(const int kurs, const int pytanie){
    PZapytanie p = aktualnyUzytkownik_->zwrocPytanie(kurs,pytanie);
    pair<const string, const string> wynik = std::make_pair<const string, const string>(p->dajPytanie(),p->dajOdpowiedz());
    return wynik;
}

const pair<string,string> Model::kolejnePytanie(){
    PZapytanie p = aktualnyUzytkownik_->zwrocKolejnePytanie();
    if (p){
        return  std::make_pair<const string, const string>(p->dajPytanie(),p->dajOdpowiedz());
    }
	else{
		//return std::make_pair<string,string>("","");
		return pair<string, string>();
	}
        
}

const int Model::zwrocIloscPytan(const int kurs, const bool aktywny){
    return aktualnyUzytkownik_->zwrocIloscPytan(kurs,aktywny);
}

void Model::ocenPytanie(const int ocena){
    aktualnyUzytkownik_->ocenZapytanie(ocena);
}

void Model::dodajKurs(const string sciezka){
    aktualnyUzytkownik_->nowyKurs(sciezka);
}

void Model::usunKurs(const int indeks){
    aktualnyUzytkownik_->usunKurs(indeks);
}

void Model::deaktywujKurs(int i){
    aktualnyUzytkownik_->deaktywujKurs(i);

}

void Model::aktywujKurs(const int kurs, const int ilosc){
    aktualnyUzytkownik_->aktywujKurs(kurs,ilosc);
}

void Model::aktualizuj(){
    aktualnyUzytkownik_->aktualizujKursy();
}

const vector<string> Model::szukajPytanie(const string wzor){
    vector<string> vc;
    wyszukania_ = aktualnyUzytkownik_->szukajPytanie(wzor);
    BOOST_FOREACH(PZapytanie &v, wyszukania_ ){ vc.push_back(v->dajPytanie()); }
    return vc;
}

const pair<string,string> Model::zwrocWyszukanePytanie(const int pytanie){
    return pair<string,string>(wyszukania_[pytanie]->dajPytanie(), wyszukania_[pytanie]->dajOdpowiedz());
}

const vector<pair<date,string> > Model::zwrocTerminarz(const int kurs){
    vector<pair<date,string> > v;
    multimap<date,PZapytanie>::iterator i, koniec;
    multimap<date,PZapytanie> w=aktualnyUzytkownik_->zwrocTerminarz(kurs);
    koniec = w.end();
    pair<date,PZapytanie> para;
    for (i=w.begin();i!=koniec;++i){
        para=*i;
        v.push_back(std::pair<date,string>(para.first,para.second->dajPytanie()));
    }
    return v;
}

 void Model::zapis(){
     try{
        if (aktualnyUzytkownik_) {
            aktualnyUzytkownik_->eksportuj();
        }
        string s="model.config";
        ofstream ofs;
        ofs.open(s.c_str());
        boost::archive::text_oarchive oa(ofs);
        oa << uzytkownicy_;
        ofs.close();
     }
     catch(...){
         throw Wyjatek("Błąd w zapisie stanu programu przed wyjściem");
     }
 }
