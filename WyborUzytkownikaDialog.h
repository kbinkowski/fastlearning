#ifndef WYBORUZYTKOWNIKADIALOG_H
#define WYBORUZYTKOWNIKADIALOG_H
#include <QDialog>
#include "ui_WyborUzytkownika.h"
#include "Widok.h"
/**
 * Klasa reprezentuje okno dialogowe, w którym można wybrać aktualnego użytkownika,
 * stworzyć nowego lub zaimportować użytkownika z pliku
 */
class WyborUzytkownikaDialog : public QDialog, private Ui::WyborUzytkownika{
    Q_OBJECT
public:
    WyborUzytkownikaDialog(Kontroler* kontroler, Widok* widok);
public slots:
    void wyswietlListaUzytkownikow(vector<string>);
private slots:
//    void on_loginComboBox_activated(string login);
    void on_buttonBox_accepted();
    void on_importButton_clicked();
    void on_nowyButton_clicked();
    void on_buttonBox_rejected();


private:
    QString login;
    Widok* widok_;
    Kontroler* kontroler_;

};

#endif // WYBORUZYTKOWNIKADIALOG_H
