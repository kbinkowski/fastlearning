#ifndef KONTROLER_H
#define KONTROLER_H
#include "Widok.h"
#include "Model.h"
#include "Stan.h"
#ifndef Q_MOC_RUN
#include <boost/foreach.hpp>
#endif // Q_MOC_RUN
#include <QObject>
#include <iostream>
#include <vector>
#include <tuple>
/**
 * Klasa reprezentuje kontroler sluzacy do komunikacji miedzy modelem a widokiem.
 *  W tym celu wykorzystany został mechanizm sygnalow i slotow.
 */
class Kontroler : public QObject{
    Q_OBJECT

public:
    explicit Kontroler();
    void dodajWidok(Widok* widok);

signals:
    void przekazListaUzytkownikow(vector<string>);
    void przekazBlad(string blad);
    void przekazListaAktywnychKursow(vector<string>);
    void przekazListaNieaktywnychKursow(vector<string>);
    void przekazZapytanie(string pytanie, string odpowiedz);
    void przekazIloscPytan(int ilosc);
    void przekazWynikiSzukania(vector<string> wyniki);
    void przekazTerminarz(vector<pair<date,string> >);
    void aktualizujStanWidok(Stan stan);

public slots:
    void wybranoNowyUzytkownik(string login);
    void wybranoImportUzytkownik(string sciezka);
    void wybranoIstniejacyUzytkownik(int indeks);
    void pytanoListaUzytkownikow();
    void pytanoListaAktywnychKursow();
    void pytanoListaNieaktywnychKursow();
    void wybranoPytaniePrzegladane(int kurs, int pytanie);
    void aktywowanoKurs(int kurs, int ilosc);
    void deaktywowanoKurs(int kurs);
    void pytanoIloscPytan(int kurs, bool aktywny);
    void szukanoPytan(string wzor);
    void rozpoczetoNauke();
    void ocenionoPytanie(int ocena);
    void wybranoNowyKurs(string plik);
    void wybranoUsuniecieKursu(int kurs);
    void wybranoWyszukanePytanie(int pytanie);
    void wyjscie();
    void pytanoTerminarz(int kurs);


private:
    Widok* widok_;
    Model* model_;
};

#endif // KONTROLER_H

