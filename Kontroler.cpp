#include "Kontroler.h"

#include <QObject>

Kontroler::Kontroler() : model_(new Model()){
}

void Kontroler::dodajWidok(Widok* widok){
    widok_ = widok;
    widok_->show();

    connect(widok_, SIGNAL(wybierzNowyUzytkownik(string)), this, SLOT(wybranoNowyUzytkownik(string)));
    connect(widok_, SIGNAL(wybierzIstniejacyUzytkownik(int)), this, SLOT(wybranoIstniejacyUzytkownik(int)));
    connect(widok_, SIGNAL(wybierzImportUzytkownik(string)), this, SLOT(wybranoImportUzytkownik(string)));
    connect(widok_, SIGNAL(pytajListaUzytkownikow()), this, SLOT(pytanoListaUzytkownikow()));
    connect(widok_, SIGNAL(pytajListaAktywnychKursow()), this, SLOT(pytanoListaAktywnychKursow()));
    connect(widok_, SIGNAL(pytajListaNieaktywnychKursow()), this, SLOT(pytanoListaNieaktywnychKursow()));
    connect(widok_, SIGNAL(wybierzPytaniePrzegladane(int,int)), this, SLOT(wybranoPytaniePrzegladane(int,int)));
    connect(widok_, SIGNAL(zacznijNauke()), this, SLOT(rozpoczetoNauke()));
    connect(widok_, SIGNAL(ocenPytanie(int)), this, SLOT(ocenionoPytanie(int)));
    connect(widok_, SIGNAL(szukajPytan(string)), this, SLOT(szukanoPytan(string)));
    connect(widok_, SIGNAL(aktywujKurs(int,int)), this, SLOT(aktywowanoKurs(int,int)));
    connect(widok_, SIGNAL(deaktywujKurs(int)), this, SLOT(deaktywowanoKurs(int)));
    connect(widok_, SIGNAL(pytajIloscPytan(int,bool)), this, SLOT(pytanoIloscPytan(int,bool)));
    connect(widok_, SIGNAL(dodajNowyKurs(string)), this, SLOT(wybranoNowyKurs(string)));
    connect(widok_, SIGNAL(usunKurs(int)), this, SLOT(wybranoUsuniecieKursu(int)));
    connect(widok_, SIGNAL(wybierzWyszukanePytanie(int)), this, SLOT(wybranoWyszukanePytanie(int)));
    connect(widok_, SIGNAL(wyjscie()), this, SLOT(wyjscie()));
    connect(widok_, SIGNAL(pytajTerminarz(int)), this, SLOT(pytanoTerminarz(int)));
    connect(this, SIGNAL(aktualizujStanWidok(Stan)), widok_, SIGNAL(aktualizujStan(Stan)));
    connect(this, SIGNAL(przekazListaUzytkownikow(vector<string>)), widok_, SIGNAL(przekazListaUzytkownikow(vector<string>)));
    connect(this, SIGNAL(przekazBlad(string)), widok_, SLOT(wyswietlBlad(string)));
    connect(this, SIGNAL(przekazListaAktywnychKursow(vector<string>)), widok_, SIGNAL(przekazListaAktywnychKursow(vector<string>)));
    connect(this, SIGNAL(przekazListaNieaktywnychKursow(vector<string>)), widok_, SIGNAL(przekazListaNieaktywnychKursow(vector<string>)));
    connect(this, SIGNAL(przekazZapytanie(string, string)), widok_, SLOT(odbierzZapytanie(string,string)));
    connect(this, SIGNAL(przekazIloscPytan(int)), widok_, SIGNAL(przekazIloscPytan(int)));
    connect(this, SIGNAL(przekazWynikiSzukania(vector<string>)), widok_, SIGNAL(przekazWynikiSzukania(vector<string>)));
    connect(this, SIGNAL(przekazTerminarz(vector<pair<date,string> >)), widok_, SIGNAL(przekazTerminarz(vector<pair<date,string> >)));
}

void Kontroler::wybranoNowyUzytkownik(string login){
    try{
        model_->stworzNowyUzytkownik(login);
        emit aktualizujStanWidok(PRZEGLADANIE);
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }

}
void Kontroler::wybranoIstniejacyUzytkownik(int indeks){
    try{

        model_->wybierzIstniejacyUzytkownik(indeks);
        emit aktualizujStanWidok(PRZEGLADANIE);
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}
void Kontroler::wybranoImportUzytkownik(string sciezka){
    try{
        model_->stworzImportUzytkownik(sciezka);
        emit aktualizujStanWidok(PRZEGLADANIE);
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}
void Kontroler::pytanoListaAktywnychKursow(){
    try{
        emit przekazListaAktywnychKursow(model_->zwrocListaAktywnychKursow());
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}
void Kontroler::pytanoListaNieaktywnychKursow(){
    try{
        emit przekazListaNieaktywnychKursow(model_->zwrocListaNieaktywnychKursow());
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}
void Kontroler::pytanoListaUzytkownikow(){
    try{
        emit przekazListaUzytkownikow(model_->zwrocListaUzytkownikow());
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}
void Kontroler::wybranoPytaniePrzegladane(int kurs, int pytanie){
    try{
        pair<string,string> pyt = model_->zwrocPytanie(kurs,pytanie);
        emit przekazZapytanie(pyt.first, pyt.second);

    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}

void Kontroler::rozpoczetoNauke(){
    try{
        model_->aktualizuj();
        pair<string,string> pyt = model_->kolejnePytanie();
        if (pyt.first.empty() && pyt.second.empty()){
            emit przekazBlad("Koniec nauki na dzisiaj!");
            emit aktualizujStanWidok(PRZEGLADANIE);
        }
        else
            emit przekazZapytanie(pyt.first, pyt.second);
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}
void Kontroler::ocenionoPytanie(int ocena){
    try{
        model_->ocenPytanie(ocena);
        pair<string,string> pyt = model_->kolejnePytanie();
        if (pyt.first.empty() && pyt.second.empty()){
            emit przekazBlad("Koniec nauki na dzisiaj!");
            emit aktualizujStanWidok(PRZEGLADANIE);
        }
        else
            emit przekazZapytanie(pyt.first, pyt.second);
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}

void Kontroler::aktywowanoKurs(int kurs, int ilosc){
    try{
        model_->aktywujKurs(kurs,ilosc);
        emit przekazListaNieaktywnychKursow(model_->zwrocListaNieaktywnychKursow());
        emit przekazListaAktywnychKursow(model_->zwrocListaAktywnychKursow());
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}

void Kontroler::deaktywowanoKurs(int kurs){
    try{
        model_->deaktywujKurs(kurs);
        emit przekazListaNieaktywnychKursow(model_->zwrocListaNieaktywnychKursow());
        emit przekazListaAktywnychKursow(model_->zwrocListaAktywnychKursow());
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}

void Kontroler::pytanoIloscPytan(int kurs, bool aktywny){
    try{
        emit przekazIloscPytan(model_->zwrocIloscPytan(kurs,aktywny));
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}

void Kontroler::szukanoPytan(string wzor){
    try{
        emit przekazWynikiSzukania(model_->szukajPytanie(wzor));
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}
void  Kontroler::wybranoWyszukanePytanie(int pytanie){
    try{
        pair<string,string> pyt = model_->zwrocWyszukanePytanie(pytanie);
        emit przekazZapytanie(pyt.first, pyt.second);
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}

void Kontroler::pytanoTerminarz(int kurs){
    try{
        emit przekazTerminarz(model_->zwrocTerminarz(kurs));
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}

void Kontroler::wybranoNowyKurs(string plik){
    try{
        model_->dodajKurs(plik);
        emit przekazListaNieaktywnychKursow(model_->zwrocListaNieaktywnychKursow());
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}
void Kontroler::wybranoUsuniecieKursu(int kurs){
    try{
        model_->usunKurs(kurs);
        emit przekazListaNieaktywnychKursow(model_->zwrocListaNieaktywnychKursow());
    }
    catch(Wyjatek e){
        emit przekazBlad(e.zwrocTekst());
    }
}

void Kontroler::wyjscie(){
    model_->zapis();
}


