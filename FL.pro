#-------------------------------------------------
#
# Project created by QtCreator 2013-05-27T00:11:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FL
TEMPLATE = app


SOURCES += main.cpp\
        Widok.cpp \
    Kontroler.cpp \
    WyborUzytkownikaDialog.cpp \
    WyborKursuDialog.cpp \
    PrzegladarkaPytanDialog.cpp \
    TerminarzDialog.cpp \
    NowyUzytkownikDialog.cpp \
    Model.cpp \
    Zapytanie.cpp \
    Kurs.cpp \
    KursProxy.cpp \
    Uzytkownik.cpp \
    UzytkownikProxy.cpp \
    testy.cpp


HEADERS  += Widok.h \
    Kontroler.h \
    WyborUzytkownikaDialog.h \
    WyborKursuDialog.h \
    PrzegladarkaPytanDialog.h \
    TerminarzDialog.h \
    Stan.h \
    NowyUzytkownikDialog.h \
    Model.h \
    Zapytanie.h \
    Kurs.h \
    KursProxy.h \
    Uzytkownik.h \
    UzytkownikProxy.h \
    Wyjatek.h

FORMS    += Widok.ui \
    WyborUzytkownika.ui \
    WyborKursu.ui \
    PrzegladarkaPytan.ui \
    Terminarz.ui \
    NowyUzytkownik.ui

RESOURCES += \
    zasoby/zasoby.qrc

INCLUDEPATH += D:\Polibuda\Programy\Zainstalowane\C++\Biblioteki\Boost\boost_1_53_0
LIBS += -LD:\Polibuda\Programy\Zainstalowane\C++\Biblioteki\Boost\boost_1_53_0\stage\lib


