#ifndef WYJATEK_H
#define WYJATEK_H
#include <exception>
#include <string>

using std::exception;
using std::string;
/**
 * Klasa służąca do obsługi wyjątków.
 */
class Wyjatek : public exception {
public:
    Wyjatek (const string tekst) : tekst_(tekst) {}
    const string zwrocTekst() const {return tekst_;}
    ~Wyjatek() throw() {}

private:
    const string tekst_;
};
#endif // WYJATEK_H
