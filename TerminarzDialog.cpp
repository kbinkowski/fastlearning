#include "TerminarzDialog.h"



TerminarzDialog::TerminarzDialog(Kontroler* kontroler, Widok* widok): QDialog(widok), widok_(widok), kontroler_(kontroler){
    setAttribute(Qt::WA_DeleteOnClose, true);
    setupUi(this);
    connect(widok_, SIGNAL(przekazTerminarz(vector<pair<date,string> >)), this, SLOT(wyswietlTerminarz(vector<pair<date,string> >)));
    connect(widok_, SIGNAL(przekazListaAktywnychKursow(vector<string>)), this, SLOT(wyswietlListaKursowUzytkownika(vector<string>)));
    connect(this, SIGNAL(rejected()), widok_, SLOT(zamknietoPodokno()));
    emit widok_->pytajListaAktywnychKursow();
    emit widok_->pytajTerminarz(0);
    show();
}
void TerminarzDialog::on_buttonBox_accepted(){
    widok_->odblokujWidok();
}

void TerminarzDialog::wyswietlListaKursowUzytkownika(vector<string> wyniki){
    kursComboBox->clear();
    BOOST_FOREACH(string &v, wyniki){kursComboBox->addItem(QString::fromStdString(v));}
}

void TerminarzDialog::wyswietlTerminarz(vector<pair<date, string> > wyniki){
    wynikList->clear();
    QString s;
    date d;
    int koniec=wyniki.size();
    for (int i=0;i!=koniec;++i){
        d=wyniki[i].first;
        s=QString::fromStdString(to_iso_extended_string(d))+" : "+QString::fromStdString(wyniki[i].second);
        wynikList->addItem(s);
    }
}

void TerminarzDialog::on_kursComboBox_currentIndexChanged(int i){
    emit widok_->pytajIloscPytan(i,true);
}
