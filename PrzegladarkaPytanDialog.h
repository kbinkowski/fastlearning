#ifndef PRZEGLADARKAPYTANDIALOG_H
#define PRZEGLADARKAPYTANDIALOG_H
#include <QDialog>
#include "ui_PrzegladarkaPytan.h"
#include "Widok.h"
/**
 * Klasa reprezentuje okno dialogowe, w którym można wyszukiwać pytania w aktywnych kursach wpisując frazę lub numer pytania.
 */
class PrzegladarkaPytanDialog : public QDialog, private Ui::PrzegladarkaPytan{
    Q_OBJECT
public:
    PrzegladarkaPytanDialog(Kontroler* kontroler, Widok* widok);
public slots:
    void wyswietlListaKursowUzytkownika(vector<string>);
    void ustawMaxIloscPytan(int ilosc);
    void wyswietlWynikiSzukania(vector<string >);

private slots:
    void on_wyszukajButton_clicked();
    void on_wybierzButton_clicked();
    void on_buttonBox_rejected();
    void on_wynikList_currentRowChanged(int i);
    void on_kursComboBox_currentIndexChanged(int i);
    void on_wzorLine_textChanged(QString wzor);
    void on_szukajButton_clicked();


private:
    Kontroler* kontroler_;
    Widok* widok_;
};

#endif // PRZEGLADARKAPYTANDIALOG_H
