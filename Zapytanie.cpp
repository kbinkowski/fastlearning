#include "Zapytanie.h"


Zapytanie::Zapytanie(const string pytanie, const string odpowiedz){
    pytanie_=pytanie;
    odpowiedz_=odpowiedz;
    wspolczynnik=2.5;
    interwal=0;
}


const string Zapytanie::dajPytanie(){
    return pytanie_;

}
const string Zapytanie::dajOdpowiedz(){
    return odpowiedz_;
}

double Zapytanie::dajWspolczynnik(){
    return wspolczynnik;
}

double Zapytanie::dajInterwal(){
    return interwal;
}

const date Zapytanie::ocenPytanie(const int ocena){
    if (ocena >= 3.0){
        wspolczynnik = wspolczynnik + (0.1 - ocena * (0.08 + ocena * 0.02));
        if(wspolczynnik < 1.3)
            wspolczynnik = 1.3;
        if (interwal == 0)
            interwal = 1;
        else if (interwal == 1)
            interwal = 6;
        else
            interwal *=wspolczynnik;
    }
    else
        interwal = 1;

    date_duration odstep(static_cast<long>(interwal));
    date data = day_clock::local_day() + odstep;
    return data;
}
