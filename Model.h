#ifndef MODEL_H
#define MODEL_H
#include "UzytkownikProxy.h"
#ifndef Q_MOC_RUN
#include <boost/foreach.hpp>
#endif // Q_MOC_RUN

using std::vector;
typedef boost::shared_ptr<UzytkownikProxy> PUzytkownikProxy;

/**
 * Klasa Model reprezentuje strukturę logiczną aplikacji, zawiera miedzy innymi zbiór użytkowników
 */
class Model
{
public:
    Model();

    void stworzNowyUzytkownik(const string login);
    void stworzImportUzytkownik(const string login);
    void wybierzIstniejacyUzytkownik(const int indeks);
    const  vector<string>  zwrocListaAktywnychKursow();
    const vector<string>  zwrocListaNieaktywnychKursow();
    const vector<string>  zwrocListaUzytkownikow();
    const pair<string,string> zwrocPytanie(const int kurs, const int pytanie);
    const pair<string,string> kolejnePytanie();
    const int zwrocIloscPytan(const int kurs, const bool aktywny);
    void ocenPytanie(const int ocena);
    void aktywujKurs(const int kurs, const int ilosc);
    void dodajKurs(const string sciezka);
    void usunKurs(const int indeks);
    void deaktywujKurs(const int);
    void aktualizuj();
    const vector<string> szukajPytanie(const string wzor);
    const pair<string,string> zwrocWyszukanePytanie(const int pytanie);
    const vector<pair<date,string> > zwrocTerminarz(const int kurs);

    void zapis();


private:


    //vector<string> loginy_;
    vector<PUzytkownikProxy> uzytkownicy_;
    PUzytkownikProxy aktualnyUzytkownik_;
    vector<PZapytanie> wyszukania_;
    string plikSerializacji_;
};

#endif // MODEL_H
