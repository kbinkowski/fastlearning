#include "Widok.h"
#include "Kontroler.h"
#include "ui_Widok.h"
#include "WyborUzytkownikaDialog.h"
#include "WyborKursuDialog.h"
#include "PrzegladarkaPytanDialog.h"
#include "TerminarzDialog.h"
#include <QMessageBox>


Widok::Widok(Kontroler* kontroler, QWidget *parent) :
    QMainWindow(parent),
    kontroler_(kontroler)
{
    setupUi(this);
    connect(this, SIGNAL(aktualizujStan(Stan)), this, SLOT(zaktualizowanoStan(Stan)));
    emit aktualizujStan(WYBOR_UZYTKOWNIKA);
}
void Widok::zablokujWidok(){
    dostosujWidok(false,false,false);
}
void Widok::odblokujWidok(){
    switch(stan){
            case NAUKA: dostosujWidok(true,true,false);
            break;
            case PRZEGLADANIE: dostosujWidok(true,false,true);
            break;
            default: dostosujWidok(true,false,false);
            break;
        }
}

void Widok::odbierzZapytanie(string pytanie, string odpowiedz){
    aktualnePytanie = pytanie;
    aktualnaOdpowiedz = odpowiedz;
    pytanieLabel->clear();
    odpowiedzLabel->clear();
    wyswietlPytanie();
}

void Widok::zaktualizowanoStan(Stan st){
    stan=st;
    switch(st){
            case NAUKA:
                Ui::Widok::statusBar->showMessage(tr("Uczysz się. Zamknięcie grozi niezapisaniem postępów"));
                dostosujWidok(true,true,false);
            break;
            case PRZEGLADANIE:
                Ui::Widok::statusBar->showMessage(tr("Przeglądasz. Nacisnij 'Rozpocznij' by zacząć naukę na dzisiaj."));
                dostosujWidok(true,false,true);
                wyczyscZapytanie();
            break;
            default:
                Ui::Widok::statusBar->showMessage(tr("Wybierz użytkownika."));
                dostosujWidok(true,false,false);
            break;
        }
}

void Widok::dostosujWidok(bool uzytkownik, bool nauka, bool przegladanie){
    sprawdzButton->setEnabled(nauka || przegladanie);
    ocenBox->setEnabled(nauka);
    actionUzytkownik->setEnabled(uzytkownik);
    actionPytania->setEnabled(przegladanie);
    actionKursy->setEnabled(przegladanie);
    actionTerminarz->setEnabled(przegladanie);
    rozpocznijButton->setEnabled(przegladanie);
}

void Widok::wyczyscZapytanie(){
    pytanieLabel->clear();
    odpowiedzLabel->clear();
}

void Widok::wyswietlBlad(string blad){
    QMessageBox::warning(this, tr("Uwaga!"),QString::fromStdString(blad));
}


void Widok::on_actionUzytkownik_triggered(){
    zablokujWidok();
    WyborUzytkownikaDialog* dialog = new WyborUzytkownikaDialog(kontroler_,this);
}

void Widok::on_actionKursy_triggered(){
    zablokujWidok();
    WyborKursuDialog* dialog = new WyborKursuDialog(kontroler_,this);
}
void Widok::on_actionPytania_triggered(){
    zablokujWidok();
    PrzegladarkaPytanDialog* dialog = new PrzegladarkaPytanDialog(kontroler_,this);
}
void Widok::on_actionTerminarz_triggered(){
    zablokujWidok();
    TerminarzDialog * dialog = new TerminarzDialog(kontroler_,this);
}
void Widok::on_actionWyjscie_triggered(){
    emit wyjscie();
}

void Widok::on_rozpocznijButton_clicked(){
    emit aktualizujStan(NAUKA);
    emit zacznijNauke();
}
void Widok::on_sprawdzButton_clicked(){
    wyswietlOdpowiedz();
}
void Widok::on_dalejButton_clicked(){
    emit ocenPytanie(ocenaSlider->value());
}

void Widok::zamknietoPodokno(){
    odblokujWidok();
}

void Widok::wyswietlPytanie(){
    pytanieLabel->setText(QString::fromStdString(aktualnePytanie));
}
void Widok::wyswietlOdpowiedz(){
    odpowiedzLabel->setText(QString::fromStdString(aktualnaOdpowiedz));
}
