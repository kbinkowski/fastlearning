#ifndef KURSPROXY_H
#define KURSPROXY_H
#include "Kurs.h"
#include "Wyjatek.h"
#ifndef Q_MOC_RUN
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#endif //Q_MOC_RUN

using std::fstream;
using std::string;
using boost::gregorian::date;
typedef boost::shared_ptr<Kurs> PKurs;
/**
 * Klasa KursProxy posredniczy w dostepie do klasy Kurs
 */
class KursProxy{

public:
    KursProxy(){}
    KursProxy(const string sciezka, const string uzytkownik);
    const string nazwa();
    const PZapytanie zapytanie(const int i);
    const PZapytanie kolejneNauka();
    const PZapytanie kolejneUtrwalanie();
    const PZapytanie kolejnePowtorka(const date data);
    const int ileNowychPytan();
    void ocenZapytanie(const int ocena);
    const int iloscZapytan();
    void ustawIloscNowychPytan(const int ilosc);
    const vector<PZapytanie> szukajPytanie(const string wzor);
    const multimap<date,PZapytanie> zwrocTerminarz();
    void aktualizuj();

private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version) {
        ar & sciezka_;
        ar & nazwa_;
        ar & kurs_;
    }
   PKurs kurs_;
   string sciezka_;
   string nazwa_;
   PKurs kurs();

};

#endif // KURSPROXY_H
