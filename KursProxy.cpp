#include "KursProxy.h"
#include "iostream"
using namespace std;


KursProxy::KursProxy(const string sciezka, const string uzytkownik){
    sciezka_=sciezka;
    unsigned f1 = sciezka.find_last_of("//");
    unsigned f2 = sciezka.find_last_of(".");
    nazwa_=sciezka.substr(f1+1,f2-f1-1);
}

PKurs KursProxy::kurs(){
    if(!kurs_){
        kurs_=PKurs(new Kurs(sciezka_));
    }
    return kurs_;
}

const string KursProxy::nazwa(){
    return nazwa_;
}
const PZapytanie KursProxy::zapytanie(const int i){
    return kurs()->zapytanie(i);
}

const PZapytanie KursProxy::kolejneNauka(){
    return kurs()->kolejneNauka();
}

const PZapytanie KursProxy::kolejneUtrwalanie(){
    return kurs()->kolejneUtrwalanie();
}

const PZapytanie KursProxy::kolejnePowtorka(const date data){
    return kurs()->kolejnePowtorka(data);
}

const int KursProxy::ileNowychPytan(){
    return kurs()->ileNowychPytan();
}

void KursProxy::ocenZapytanie(const int ocena){
    kurs()->ocenZapytanie(ocena);
}

const int KursProxy::iloscZapytan(){
    return kurs()->iloscZapytan();
}

void KursProxy::ustawIloscNowychPytan(const int ilosc){
    kurs()->ustawIloscNowychPytan(ilosc);
}

const vector<PZapytanie> KursProxy::szukajPytanie(const string wzor){
    return kurs()->szukajPytanie(wzor);
}

const multimap<date,PZapytanie> KursProxy::zwrocTerminarz(){
    return kurs()->zwrocTerminarz();
}

void KursProxy::aktualizuj(){
    kurs()->aktualizuj();
}
