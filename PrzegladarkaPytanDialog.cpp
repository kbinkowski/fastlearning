#include "PrzegladarkaPytanDialog.h"
#ifndef Q_MOC_RUN
#include <boost/foreach.hpp>
#endif // Q_MOC_RUN

PrzegladarkaPytanDialog::PrzegladarkaPytanDialog(Kontroler* kontroler, Widok* widok): QDialog(widok), widok_(widok), kontroler_(kontroler){
    setAttribute(Qt::WA_DeleteOnClose, true);
    setupUi(this);
    connect(widok_, SIGNAL(przekazListaAktywnychKursow(vector<string>)), this, SLOT(wyswietlListaKursowUzytkownika(vector<string>)));
    connect(widok_, SIGNAL(przekazIloscPytan(int)), this, SLOT(ustawMaxIloscPytan(int)));
    connect(this,   SIGNAL(rejected()), widok_, SLOT(zamknietoPodokno()));
    connect(widok_, SIGNAL(przekazWynikiSzukania(vector<string>)), this, SLOT(wyswietlWynikiSzukania(vector<string>)));
    emit widok_->pytajListaAktywnychKursow();
    show();
}
//Publiczne sloty:
void PrzegladarkaPytanDialog::wyswietlListaKursowUzytkownika(vector<string> vc){
    kursComboBox->clear();
    BOOST_FOREACH(string &v, vc){kursComboBox->addItem(QString::fromStdString(v));}
}
void PrzegladarkaPytanDialog::ustawMaxIloscPytan(int ilosc){
    pytanieSpinBox->setMaximum(ilosc-1);
}

//private SLOTS:
void PrzegladarkaPytanDialog::on_wybierzButton_clicked(){
    emit widok_->wybierzPytaniePrzegladane(kursComboBox->currentIndex(), pytanieSpinBox->value());
    widok_->odblokujWidok();
    close();
}
void PrzegladarkaPytanDialog::on_buttonBox_rejected(){  
    widok_->odblokujWidok();
    close();
}
void PrzegladarkaPytanDialog::on_wyszukajButton_clicked(){
    int i = wynikList->currentRow();
    emit widok_->wybierzWyszukanePytanie(i);
    close();
}
void PrzegladarkaPytanDialog::on_kursComboBox_currentIndexChanged(int i){
    emit widok_->pytajIloscPytan(i,true);
}
void PrzegladarkaPytanDialog::on_wynikList_currentRowChanged(int i){
    if (i<0)
        wyszukajButton->setEnabled(false);
    else
        wyszukajButton->setEnabled(true);
}

void PrzegladarkaPytanDialog::on_wzorLine_textChanged(QString wzor){
    if (wzor.isEmpty())
        szukajButton->setEnabled(false);
    else
        szukajButton->setEnabled(true);
}

void PrzegladarkaPytanDialog::on_szukajButton_clicked(){
    emit widok_->szukajPytan((wzorLine->text()).toStdString());
}
void PrzegladarkaPytanDialog::wyswietlWynikiSzukania(vector<string> wyniki){
    wynikList->clear();
    BOOST_FOREACH(string &v, wyniki){ wynikList->addItem(QString::fromStdString(v));}
}
