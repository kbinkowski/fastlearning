#ifndef WYBORKURSUDIALOG_H
#define WYBORKURSUDIALOG_H
#include <QDialog>
#include "ui_WyborKursu.h"
#include "Widok.h"
/**
 * Klasa reprezentuje okno dialogowe, które pozwala na dodawanie, usuwanie, aktywację, dezaktywację Kursów.
 */
class WyborKursuDialog : public QDialog, private Ui::WyborKursu{
    Q_OBJECT
public:
    WyborKursuDialog(Kontroler* kontroler, Widok* widok);

public slots:
    void wyswietlListaAktywnychKursow(vector<string>);
    void wyswietlListaNieaktywnychKursow(vector<string>);
    void ustawMaxIloscPytan(int ilosc);

private slots:
    void on_buttonBox_accepted();
    void on_aktywujButton_clicked();
    void on_deaktywujButton_clicked();
    void on_nieaktywneList_currentRowChanged();
    void on_dodajButton_clicked();
    void on_usunButton_clicked();

private:
    Kontroler* kontroler_;
    Widok* widok_;
};

#endif // WYBORKURSUDIALOG_H
