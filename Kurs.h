#ifndef KURS_H
#define KURS_H
#include "Zapytanie.h"
#include <fstream>
#include <vector>
#include <list>
#include <map>
#ifndef Q_MOC_RUN
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/list.hpp>
#include <boost/date_time/gregorian/greg_serialize.hpp>
#include <boost/regex.hpp>
#include <boost/foreach.hpp>
#endif // Q_MOC_RUN



typedef boost::shared_ptr<Zapytanie> PZapytanie;
using std::fstream;
using std::ifstream;
using std::string;
using std::vector;
using std::list;
using std::multimap;
using boost::gregorian::date;
using boost::gregorian::day_clock;
using boost::gregorian::date_duration;
/**
 * Klasa kurs reprezentuje zbior zapytan.
 */
class Kurs{
public:
    Kurs(){}
    Kurs(const string sciezka);
    const PZapytanie zapytanie(const int i);
    const PZapytanie kolejneNauka();
    const PZapytanie kolejneUtrwalanie();
    const PZapytanie kolejnePowtorka(const date data);
    const int ileNowychPytan();
    void ocenZapytanie(const int ocena);
    const int iloscZapytan();
    void ustawIloscNowychPytan(const int ilosc);
    void aktualizuj();
    const vector<PZapytanie> szukajPytanie(const string wzor);
    const multimap<date,PZapytanie> zwrocTerminarz();

private:
    void utrwal(PZapytanie doUtrwalenia);
    void powtorz(date data,PZapytanie doPowtorki);

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version) {
        ar & zapytania_;
        ar & naukaZapytania_;
        ar & powtorkaZapytania_;
    }

    PZapytanie aktualneZapytanie_;
    vector<PZapytanie> zapytania_;
    list<PZapytanie> naukaZapytania_;
    list<PZapytanie> utrwalanieZapytania_;
    multimap<date, PZapytanie> powtorkaZapytania_;
    int iloscNowychPytan_;
};

#endif // KURS_H
