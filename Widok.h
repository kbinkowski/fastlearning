#ifndef WIDOK_H
#define WIDOK_H
#ifndef Q_MOC_RUN
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/foreach.hpp>
#endif // Q_MOC_RUN
#include <QMainWindow>
#include "ui_Widok.h"
#include "Stan.h"
#include <string>
#include <vector>
#include <tuple>
#include <map>

using std::string;
using std::vector;
using std::map;
using std::pair;
using std::tuple;
using boost::gregorian::date;

class Kontroler;
/**
 * Klasa reprezentuje okno główne programu oraz pośredniczy w przekazywaniu akcji użytkownika do Kontrolera.
 */
class Widok : public QMainWindow, private Ui::Widok{
    Q_OBJECT
    
public:
    explicit Widok(Kontroler* kontroler ,QWidget *rodzic = NULL);
    void zablokujWidok();
    void odblokujWidok();


signals:
    void wybierzNowyUzytkownik(string);
    void wybierzImportUzytkownik(string);
    void wybierzIstniejacyUzytkownik(int indeks);
    void pytajListaUzytkownikow();
    void pytajListaAktywnychKursow();
    void pytajListaNieaktywnychKursow();
    void wybierzPytaniePrzegladane(int kurs, int pytanie);
    void aktywujKurs(int kurs, int ilosc);
    void deaktywujKurs(int kurs);
    void aktualizujStan(Stan st);
    void pytajIloscPytan(int kurs, bool aktywny);
    void szukajPytan(string wzor);
    void zacznijNauke();
    void ocenPytanie(int ocena);
    void dodajNowyKurs(string plik);
    void usunKurs(int kurs);
    void przekazListaUzytkownikow(vector<string>);
    void przekazListaAktywnychKursow(vector<string>);
    void przekazListaNieaktywnychKursow(vector<string>);
    void przekazIloscPytan(int ilosc);
    void przekazWynikiSzukania(vector<string> wyniki);
    void wybierzWyszukanePytanie(int i);
    void przekazTerminarz(vector<pair<date,string> > wyniki);
    void pytajTerminarz(const int kurs);
    void wyjscie();




public slots:
    void odbierzZapytanie(string pytanie, string odpowiedz);
    void zaktualizowanoStan(Stan st);
    void zamknietoPodokno();
    void wyswietlBlad(string blad);

private slots:
    void on_actionUzytkownik_triggered();
    void on_actionKursy_triggered();
    void on_actionPytania_triggered();
    void on_actionTerminarz_triggered();
    void on_rozpocznijButton_clicked();
    void on_sprawdzButton_clicked();
    void on_dalejButton_clicked();
    void on_actionWyjscie_triggered();


private:
    void wyswietlPytanie();
    void wyswietlOdpowiedz();
    void dostosujWidok(bool uzytkownik, bool nauka, bool przegladanie);
    void wyczyscZapytanie();

    Kontroler* kontroler_;
    string aktualnePytanie;
    string aktualnaOdpowiedz;
    Stan stan;
};

#endif // WIDOK_H
