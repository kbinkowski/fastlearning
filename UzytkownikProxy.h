#ifndef UZYTKOWNIKPROXY_H
#define UZYTKOWNIKPROXY_H
#include "Uzytkownik.h"
#include "Stan.h"

#ifndef Q_MOC_RUN
#include <boost/shared_ptr.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#endif //Q_MOC_RUN
#include <vector>
#include <string>
#include <fstream>

typedef boost::shared_ptr<Uzytkownik> PUzytkownik;
typedef boost::shared_ptr<Zapytanie> PZapytanie;
using namespace std;
/**
 * Klasa pośrednicząca dostęp do klasy Użytkownik.
 */
class UzytkownikProxy
{
public:

    UzytkownikProxy(){}
    UzytkownikProxy(string sciezka, bool import = false);
    string zwrocNazwe();
    vector<PKursProxy> zwrocAktywneKursy();
    vector<PKursProxy> zwrocNieaktywneKursy();
    void nowyKurs(string sciezka);
    void usunKurs(const int indeks);
    void aktywujKurs (const int indeks, const int iloscPytan);
    void deaktywujKurs (const int indeks);
    void aktualizujKursy ();
    void ocenZapytanie(const int ocena);
    PZapytanie zwrocKolejnePytanie();
    PZapytanie zwrocPytanie(int kurs, int indeks);
    const int zwrocIloscPytan(const int indeks, bool aktywny);
    const vector<PZapytanie> szukajPytanie(const string wzor);
    const multimap<date,PZapytanie> zwrocTerminarz(const int kurs);

    void eksportuj();

private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version) {
            ar & plikSerializacji_;
            ar & nazwa_;
            ar & uzytkownik_;
        }
    string nazwa_;
    PUzytkownik zwrocUzytkownika();
    PUzytkownik uzytkownik_;
    string plikSerializacji_;
};

#endif // UZYTKOWNIKPROXY_H
