#ifndef NOWYUZYTKOWNIK_H
#define NOWYUZYTKOWNIK_H
#include "Widok.h"
#include "ui_NowyUzytkownik.h"
#include <QDialog>

/**
 * Klasa reprezentuje okno dialogowe, w którym jest możliwość tworzenia nowego użytkownika.
 */
class NowyUzytkownikDialog : public QDialog, private Ui::NowyUzytkownik{
    Q_OBJECT   
public:
    NowyUzytkownikDialog(Kontroler* kontroler, Widok* widok);
private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_lineEdit_textChanged(QString);

private:
    Kontroler* kontroler_;
    Widok* widok_;
};

#endif // NOWYUZYTKOWNIK_H
