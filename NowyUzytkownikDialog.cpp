#include "NowyUzytkownikDialog.h"

NowyUzytkownikDialog::NowyUzytkownikDialog(Kontroler* kontroler, Widok* widok): QDialog(widok), widok_(widok), kontroler_(kontroler){
    setAttribute(Qt::WA_DeleteOnClose, true);
    setupUi(this);
    connect(this, SIGNAL(rejected()), widok_, SLOT(zamknietoPodokno()));
    buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    show();
}

void NowyUzytkownikDialog::on_buttonBox_accepted(){
    emit widok_->wybierzNowyUzytkownik((lineEdit->text()).toStdString());
}
void NowyUzytkownikDialog::on_buttonBox_rejected(){
    widok_->odblokujWidok();
}

void NowyUzytkownikDialog::on_lineEdit_textChanged(QString st){
    if (st.isEmpty())
        buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    else
        buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
}
