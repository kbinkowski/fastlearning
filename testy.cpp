#ifdef __unix__
#define BOOST_TEST_DYN_LINK
#endif

#define BOOST_TEST_MAIN
#define BOOST_TEST_MODULE TestySupermemo


#include <boost/test/unit_test.hpp>

#include "Model.h"
using namespace boost::unit_test;

BOOST_AUTO_TEST_CASE( test_zapytanie )
{
    string pytanie("pytanie");
    string odpowiedz ("odpowiedz");
    Zapytanie z(pytanie, odpowiedz);

    BOOST_CHECK( z.dajPytanie() == pytanie);
    BOOST_CHECK( z.dajOdpowiedz() == odpowiedz);
    z.ocenPytanie(5);
    //BOOST_CHECK( z.interwal==1);
}
BOOST_AUTO_TEST_CASE( test_kurs )
{
    Kurs k ("C:/Programy/Dropbox/ProjektZPR/FL/kursy/Idiomy.qaa");
    k.ustawIloscNowychPytan(6);
    BOOST_CHECK(k.ileNowychPytan()==6);
    BOOST_CHECK(k.iloscZapytan()>0);
    //BOOST_CHECK((k.szukajPytanie("a").size()>0);

}
BOOST_AUTO_TEST_CASE( test_kursProxy )
{
    //KursProxy k ("C:/Programy/Dropbox/ProjektZPR/FL/kursy/Idiomy.qaa");
    //BOOST_CHECK(k.nazwa()=="Idiomy");
    //k.ustawIloscNowychPytan(4);
    //BOOST_CHECK(k.ileNowychPytan()==4);
   // BOOST_CHECK((k.szukajPytanie("a").size()>0);
}
BOOST_AUTO_TEST_CASE( test_uzytkownik )
{
    Uzytkownik u;
    //u.nowyKurs("C:/Programy/Dropbox/ProjektZPR/FL/kursy/Idiomy.qaa");
    BOOST_CHECK((u.zwrocNieaktywneKursy()).size()>0);
    u.aktywujKurs(0,5);
    BOOST_CHECK((u.zwrocAktywneKursy()).size()>0);
    //BOOST_CHECK(u.zwrocIloscPytan(0)==5);
    u.deaktywujKurs(0);
    BOOST_CHECK((u.zwrocAktywneKursy()).size()==0);
}
BOOST_AUTO_TEST_CASE( test_uzytkownikProxy )
{
    UzytkownikProxy u("user");
    BOOST_CHECK(u.zwrocNazwe()=="user");
    u.nowyKurs("C:/Programy/Dropbox/ProjektZPR/FL/kursy/Idiomy.qaa");
    BOOST_CHECK((u.zwrocNieaktywneKursy()).size()>0);
    u.aktywujKurs(0,5);
    BOOST_CHECK((u.zwrocAktywneKursy()).size()>0);
    //BOOST_CHECK(u.zwrocIloscPytan(0)==5);
    u.deaktywujKurs(0);
    BOOST_CHECK((u.zwrocAktywneKursy()).size()==0);

}
BOOST_AUTO_TEST_CASE( test_model )
{
    Model m();
    //m.stworzNowyUzytkownik("login");
    //m.wybierzIstniejacyUzytkownik(0);
   // BOOST_CHECK( zwrocListaUzytkownikow().size()!=0);
}
