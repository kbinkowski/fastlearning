#include "WyborKursuDialog.h"
#include <QFileDialog>
#ifndef Q_MOC_RUN
#include <boost/foreach.hpp>
#endif // Q_MOC_RUN

WyborKursuDialog::WyborKursuDialog(Kontroler* kontroler, Widok* widok): QDialog(widok), widok_(widok), kontroler_(kontroler){
    setAttribute(Qt::WA_DeleteOnClose, true);
    setupUi(this);
    connect(widok_, SIGNAL(przekazListaAktywnychKursow(vector<string>)), this, SLOT(wyswietlListaAktywnychKursow(vector<string>)));
    connect(widok_, SIGNAL(przekazListaNieaktywnychKursow(vector<string>)), this, SLOT(wyswietlListaNieaktywnychKursow(vector<string>)));
    connect(widok_, SIGNAL(przekazIloscPytan(int)), this, SLOT(ustawMaxIloscPytan(int)));
    connect(this, SIGNAL(rejected()), widok_, SLOT(zamknietoPodokno()));
    emit widok_->pytajListaNieaktywnychKursow();
    emit widok_->pytajListaAktywnychKursow();
    show();
}

void WyborKursuDialog::wyswietlListaAktywnychKursow(vector<string> vc){
    aktywneList->clear();
    BOOST_FOREACH(string &v, vc){ aktywneList->addItem(QString::fromStdString(v));}
}

void WyborKursuDialog::wyswietlListaNieaktywnychKursow(vector<string> vc){
    nieaktywneList->clear();
    BOOST_FOREACH(string &v, vc){nieaktywneList->addItem(QString::fromStdString(v));}
}

void WyborKursuDialog::ustawMaxIloscPytan(int ilosc){
    iloscPytanSpinBox->setMaximum(ilosc-1);
}

//private slots:
void WyborKursuDialog::on_buttonBox_accepted(){
    widok_->odblokujWidok();
}
void WyborKursuDialog::on_aktywujButton_clicked(){
    if (nieaktywneList->currentRow()>=0)
        emit widok_->aktywujKurs(nieaktywneList->currentRow(), iloscPytanSpinBox->value());
}

void WyborKursuDialog::on_deaktywujButton_clicked(){
    if (aktywneList->currentRow()>=0)
        emit widok_->deaktywujKurs(aktywneList->currentRow());
}

void WyborKursuDialog::on_nieaktywneList_currentRowChanged(){
    if (nieaktywneList->currentRow()>=0){
        emit widok_->pytajIloscPytan(nieaktywneList->currentRow(), false);
        usunButton->setEnabled(true);
    }
    else
        usunButton->setEnabled(false);
}
void WyborKursuDialog::on_dodajButton_clicked(){
    this->setEnabled(false);
    QString plik = QFileDialog::getOpenFileName(this, tr("Wybierz plik kursu"),tr(""),tr("Plik importu(*.qaa)"));
    this->setEnabled(true);
    if (!plik.isNull())
        emit widok_->dodajNowyKurs(plik.toStdString());
}

void WyborKursuDialog::on_usunButton_clicked(){
    if(nieaktywneList->currentRow()>=0)
        emit widok_->usunKurs(nieaktywneList->currentRow());
}

