#ifndef TERMINARZDIALOG_H
#define TERMINARZDIALOG_H
#include <QDialog>
#include "ui_Terminarz.h"
#include "Widok.h"
 /**
  *Klasa reprezentuje okno dialogowe, w którym znajduje się lista pytań oraz odpowiadająch im dat
  *kolejnego wyświetlenia oraz kalendarz.
  */

class TerminarzDialog: public QDialog, private Ui::Terminarz{
    Q_OBJECT
public:
    TerminarzDialog(Kontroler* kontroler, Widok* widok);

public slots:
    void wyswietlTerminarz(vector<pair<date,string> > wyniki);
    void wyswietlListaKursowUzytkownika(vector<string> wyniki);
private slots:
    void on_buttonBox_accepted();
    void on_kursComboBox_currentIndexChanged(int i);
private:
    Kontroler* kontroler_;
    Widok* widok_;
};

#endif // TERMINARZDIALOG_H
