#include "WyborUzytkownikaDialog.h"
#include "NowyUzytkownikDialog.h"
#include <QFileDialog>
#ifndef Q_MOC_RUN
#include <boost/foreach.hpp>
#endif // Q_MOC_RUN

WyborUzytkownikaDialog::WyborUzytkownikaDialog(Kontroler* kontroler, Widok* widok): QDialog(widok), widok_(widok), kontroler_(kontroler){
    setAttribute(Qt::WA_DeleteOnClose, true);
    setupUi(this);
    connect(widok_, SIGNAL(przekazListaUzytkownikow(vector<string>)), this, SLOT(wyswietlListaUzytkownikow(vector<string>)));
    connect(this, SIGNAL(rejected()), widok_, SLOT(zamknietoPodokno()));
    emit widok_->pytajListaUzytkownikow();
    show();

}

void WyborUzytkownikaDialog::on_buttonBox_accepted(){
    emit widok_->wybierzIstniejacyUzytkownik(loginComboBox->currentIndex());
}
void WyborUzytkownikaDialog::on_buttonBox_rejected(){
    widok_->odblokujWidok();
}

void WyborUzytkownikaDialog::on_importButton_clicked(){
    this->close();
    QString plik = QFileDialog::getOpenFileName(this, tr("Wybierz plik Użytkownika"),tr(""),tr("Plik importu(*.user)"));
    if (!plik.isNull())
        emit widok_->wybierzImportUzytkownik(plik.toStdString());
    else
        widok_->odblokujWidok();

}
void WyborUzytkownikaDialog::on_nowyButton_clicked(){
    this->close();
    NowyUzytkownikDialog* dialog = new NowyUzytkownikDialog(kontroler_, widok_);
}

//Publiczne sloty:
void WyborUzytkownikaDialog::wyswietlListaUzytkownikow(vector<string> vc){
    loginComboBox->clear();
    BOOST_FOREACH(string &v, vc){ loginComboBox->addItem(QString::fromStdString(v));}
}
